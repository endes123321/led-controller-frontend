import serial, streams, strutils, json
export serial.serialstream, serial.serialport_common

type 
    ComStatus* = enum NeedsUpgrade, Busy, NotResponding, Ready
    DeviceError* = ref object of system.Exception

const COMPATIBLE_VERSION* = '1'

proc check_cmd(port: SerialStream): bool =
    var resp: string;
    return port.readLine(resp) and resp == "C: OK"

template unserial_ver(ver: string): int =
    parseInt(ver[0] & ver[2] & ver[4])


proc isResponding(port: SerialStream): bool =
    port.write("uc")
    return port.readAll() != ""

proc isCompatibleVersion(port: SerialStream): bool =
    port.write("vg")
    var resp_string: string
    discard port.readLine(resp_string)
    return port.check_cmd() and resp_string[0] == COMPATIBLE_VERSION

proc begin*(port: SerialStream): ComStatus =
    #if not port.isOpen(): return Busy
    port.setTimeouts(10000, 10000)
    if not port.isResponding(): return NotResponding
    if not port.isCompatibleVersion(): return NeedsUpgrade
    return Ready
        

template cmd_one_line(port: SerialStream, cmd: string): string =
    port.write(cmd)
    var presp: string;
    if not port.readLine(presp) or not port.check_cmd():
        raise DeviceError()
    presp

template cmd_no_resp(port: SerialStream, cmd: string) =
    port.write(cmd)
    if not port.check_cmd():
        raise DeviceError()

proc get_version*(port: SerialStream): int =
    let r = cmd_one_line(port, "vg")
    return r.unserial_ver()

proc get_effects*(port: SerialStream): JsonNode =
    return cmd_one_line(port, "eg").parseJson()

proc start_scene*(port: SerialStream, scene = -1): int =
    if scene == -1:
        return cmd_one_line(port, "ig").parseInt()
    else:
        cmd_no_resp(port, "is " & $scene)
        return scene

proc change_rate*(port: SerialStream, rate = -1): int =
    if rate == -1:
        return cmd_one_line(port, "cg").parseInt()
    else:
        cmd_no_resp(port, "cs " & $rate)
        return rate

proc scene*(port: SerialStream, scene = -1): int =
    if scene == -1:
        return cmd_one_line(port, "sc").parseInt()
    else:
        cmd_no_resp(port, "ss " & $scene)
        return scene

proc send_scene*(port: SerialStream, scene: JsonNode, save = false) =
    if save:
        cmd_no_resp(port, "sw " & $scene)
    else:
        cmd_no_resp(port, "ss " & $scene)

proc recive_scene*(port: SerialStream, scene: int = -1): JsonNode =
    discard
    #TODO