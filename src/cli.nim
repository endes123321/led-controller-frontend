import os, com, updater, strutils, json

const doc = """
LED Controller Frontend.

Usage:
  led_cli update --port=<port>
  led_cli device <cmd> --port=<port>
  led_cli device <cmd> <arg> --port=<port>
  led_cli scene_content get <num> --port=<port>
  led_cli scene_content set <content> [--save] --port=<port>
  led_cli (-h | --help)
  led_cli (-v | --version)

Options:
  -h --help     Show this screen.
  -v --version  Show version.
  --port=<port> The connection port.
  --save        Save the scene in the device.
"""

template warp(code: untyped) =
    try:
        code
        quit()
    except DeviceError:
        quit("Error on the device")

when isMainModule:
    import docopt
    let args = docopt(doc, version = "LED Controller Frontend 0.1.0\nProtocol " & com.COMPATIBLE_VERSION)

    if args["--port"] and not args["update"]:
        var port = newSerialStream($args["--port"], 9600, Parity.None, 8, StopBits.One)
        sleep(1000)
        let status = port.begin()
        if status != Ready:
            quit("Device " & $status)
        if args["device"]:
            var arg = -1
            if args["<arg>"]: arg = parseInt($args["<arg>"])

            warp:
                case $args["<cmd>"]:
                    of "start_scene":
                        echo port.start_scene(arg)
                    of "change_rate":
                        echo port.change_rate(arg)
                    of "scene":
                        echo port.scene(arg)
                    of "effects":
                        echo port.get_effects()
                    of "version":
                        echo port.get_version()
            
        elif args["scene_content"]:
            warp:
                if args["get"]:
                    echo port.recive_scene(parseInt($args["num"]))
                else:
                    port.send_scene(parseJson($args["<content>"]), args["--save"])
        else:
            quit(1)

    elif args["update"] and args["--port"]:
        warp:
            if update($args["--port"]):
                echo "Updated succesful!"

    else:
        quit("No port specify")

    #dispatchMulti([connect], [update], [get_ver]#[, [port.get_effects], [port.start_scene], [port.change_rate], [port.scene], [port.send_scene]]#)