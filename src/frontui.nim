import os, json, strutils, ui, uibuilderpkg/codegen, serial/utils, com

const path = joinPath(staticExec("pwd"), "views/main.glade")
var device: SerialStream

init()
build(path)


proc try_connect() =
    try:
        device = newSerialStream(ports.text, 9600, Parity.None, 8, StopBits.One)
    except InvalidSerialPortError:
        frontwindow.msgBoxError("Invalid Port", "El nombre del puerto es invalido.")
        return;
    except OSError:
        frontwindow.msgBoxError("System error", "Error en el sistema: " & getCurrentExceptionMsg() & ".\nComprueba que la App tenga permisos para abrir el puerto.")
        return;
    sleep(2000)
    let status = device.begin()
    if status != Ready:
        connect_err.show()
        case status:
            of NeedsUpgrade: frontwindow.msgBoxError($status, "El dispositivo necesita ser actualizado para ser utilizado.")
            of Busy: frontwindow.msgBoxError($status, "El puerto esta ocupado por otra aplicacion.")
            of NotResponding: frontwindow.msgBoxError($status, "El dispositivo no responde.")
            of Ready: echo "This should not have happen!"
        return
    else:
        connect.disable()
        ports.disable()
        execute.enable()
        get.enable()
    
    #[for ef in device.get_effects():
        case ef.getStr():
            of "fire":
                fire_box.enable()
            of "rainbow":
                rainbow_box.enable()
            of "solid":
                solid_box.enable()
            of "twinkle":
                twikle_box.enable()]#

proc unserialize_effects(scene: JsonNode) =
    case scene["effect"]["name"].getStr():
        of "fire":
            fire_cooling.value = scene["effect"]["cooling"].getInt
            fire_spark.value = scene["effect"]["sparking"].getInt
            #fire_foward = scene["effect"]["forward"].getBool
        of "rainbow":
            rainbow_hue.value = scene["effect"]["delta_hue"].getInt
            rainbow_rate.value = scene["effect"]["rate"].getInt
        of "solid":
            #TODO: RGB Jarray to hex color
            #solid_hex.value = scene["effect"]["color_rgb"].getHex
            solid_rate.value = scene["effect"]["rate"].getInt
        of "twinkle":
            twikle_initbrighthen.value = scene["effect"]["initial_brightness"].getInt
            twikle_maxbrighten.value = scene["effect"]["max_brightness"].getInt
            twikle_brightenrate.value = scene["effect"]["brighten_rate"].getInt
            twikle_faderate.value = scene["effect"]["fade_rate"].getInt
            twikle_density.value = scene["effect"]["density"].getInt

proc serialize_effect(eType: string): JsonNode =
    result = newJObject()
    result.add("effect", newJObject())
    result["effect"]["name"] = eType.newJString()
    case eType:
        of "fire":
            result["effect"]["cooling"] = newJInt(fire_cooling.value)
            result["effect"]["sparking"] = newJInt(fire_spark.value)
            #fire_foward = scene["effect"]["forward"].getBool
        of "rainbow":
            result["effect"]["delta_hue"] = newJInt(rainbow_hue.value)
            result["effect"]["rate"] = newJInt(rainbow_rate.value)
        of "solid":
            #TODO: hex color to RGB Jarray
            #solid_hex.value = scene["effect"]["color_rgb"].getHex
            result["effect"]["rate"] = newJInt(solid_rate.value)
        of "twinkle":
            result["effect"]["initial_brightness"] = newJInt(twikle_initbrighthen.value)
            result["effect"]["max_brightness"] = newJInt(twikle_maxbrighten.value)
            result["effect"]["brighten_rate"] = newJInt(twikle_brightenrate.value)
            result["effect"]["fade_rate"] = newJInt(twikle_faderate.value)
            result["effect"]["density"] = newJInt(twikle_density.value)

proc execute_effect(name: string): proc() =
    return proc() =
        device.send_scene(name.serialize_effect())

proc save_effect(name: string): proc() =
    return proc() =
        discard device.scene(scenes.text()[7].int - int('0'))
        device.send_scene(name.serialize_effect(), true)

proc make() = 
    frontwindow.title = "Led Controller"
    connect_err.hide()
    for port in listSerialPorts():
        ports.add($port)
    #[fire_box.disable()
    rainbow_box.disable()
    solid_box.disable()
    twikle_box.disable()]#
    execute.disable()
    get.disable()

    connect.onClick = try_connect
    execute.onClick = proc() =
        if scenes.text().startsWith("Escena "):
            discard device.scene(scenes.text()[7].int - int('0'))
        else:
            frontwindow.msgBoxError("Invalid scene number", "El numero de escena es invalido.")
    get.onClick = proc() =
        if scenes.text().startsWith("Escena "):
            unserialize_effects(device.recive_scene(scenes.text()[7].int - int('0')))
        else:
            frontwindow.msgBoxError("Invalid scene number", "El numero de escena es invalido.")

    fire_execute.onClick = execute_effect("fire")
    rainbow_execute.onClick = execute_effect("rainbow")
    solid_execute.onClick = execute_effect("solid")
    twikle_execute.onClick = execute_effect("twinkle")
    fire_save.onClick = save_effect("fire")
    rainbow_save.onClick = save_effect("rainbow")
    solid_save.onClick = save_effect("solid")
    twikle_save.onClick = save_effect("twinkle")

when isMainModule:
    make()
    mainLoop()
