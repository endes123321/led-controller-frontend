# Package

version     = "0.1.0"
author      = "endes"
description = "Frontend for led controller."
license     = "GPL v3.0"
srcDir      = "src"

# Deps

requires "cligen"
requires "serial >= 1.1.2"
requires "uibuilder >= 0.2.1"
requires "nim >= 0.19.0"